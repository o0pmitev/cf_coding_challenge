import React from 'react';
import {DataContextProvider}  from './components/courses/dataContext'
import 'bootstrap/dist/css/bootstrap.min.css';
import Courses from './components/courses/Courses'


const  App = () => {
  return (
          <div className="container">
            <DataContextProvider>
              <Courses />
            </DataContextProvider>
          </div>
  );
}

export default App;
