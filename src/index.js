import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import FullStackCourseDetails from './components/courses/fullStackImmersion/FullStackCourseDetails'
import VoiceUserCourseDetails from './components/courses/voiceUserInterfaceDesign/VoiceUserInterfaceDesign'
import {
    BrowserRouter as Router,
    Route,
  } from "react-router-dom";

ReactDOM.render(
    <Router>
        <div>
            <Route exact path='/' component={App} />
            <Route path='/full-stack-course-details' component={FullStackCourseDetails} />
            <Route path='/voice-user-course-details' component={VoiceUserCourseDetails} />
         </div>
    </Router>,  
    document.getElementById('root')
);

