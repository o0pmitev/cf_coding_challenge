import React, {useContext} from 'react'
import FullStackImmersion from './fullStackImmersion/FullStackImmersion'
import VoiceUserInterfaceDesign from './voiceUserInterfaceDesign/VoiceUserInterfaceDesign'
import {DataContext} from './dataContext'

const Courses = () => {
  
  const {data,  getCourses, fullStackCourseDetails, voiceCourseDetails} = useContext(DataContext)

  let voiceCourse = data.filter(course => (
    course.slug === 'voice-user-interface-design-with-amazon-alexa'
  ))
  
  let fullStackCourse = data.filter(course => (
    course.slug === 'full-stack-immersion'
  ))
    console.log(fullStackCourse)
    console.log(voiceCourse)

  return(
    <div className="courses-container">
      <FullStackImmersion data={fullStackCourse} getCourses={getCourses} fullStackCourseDetails={fullStackCourseDetails}/>
      <VoiceUserInterfaceDesign data={voiceCourse} getCourses={getCourses} voiceCourseDetails={voiceCourseDetails}/>
    </div>
  )
}


export default Courses