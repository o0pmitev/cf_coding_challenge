import React from 'react'
import { Link } from "react-router-dom"

const FullStackImmersion = ({data, getCourses, fullStackCourseDetails}) => {

  let jsx = data.map(element => (
    <div key={element.slug} className="course-container">
      <h2>{element.title}</h2>
      <p>{element.author}</p>
      <p>{element.next_start_formatted}</p>
      {/* <Link to="/full-stack-course-details"> */}
        <button onClick={() => getCourses(element.slug)}>
          View details
        </button>
      {/* </Link> */}
    </div>
  ))
  console.log(fullStackCourseDetails)
  return(

   <> 
    {jsx}
   </>
  )
}


export default FullStackImmersion