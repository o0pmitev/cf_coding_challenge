import React from 'react'
import { Link } from "react-router-dom"


const VoiceUserInterfaceDesign = ({data, getCourses, voiceCourseDetails}) => {

 console.log(voiceCourseDetails)
  let jsx = data.map(element => (
    <div key={element.slug} className="course-container">
      <h2>{element.title}</h2>
      <p>{element.author}</p>
      <p>{element.next_start_formatted}</p>
        <button onClick={() => getCourses(element.slug)}>View details</button>
    </div>
  ))  
  return(
      
      <>
        {jsx}
      </>
  )
}


export default VoiceUserInterfaceDesign