import React, { createContext, useState, useEffect } from 'react'
import axios from 'axios'

const DataContext = createContext()

const DataContextProvider = (props) => {

  const [data, setData] = useState([])
  const [slug, setSlug] = useState('')
  const [fullStackCourseDetails, setFullStackCourseCourseDetails] = useState({})
  const [voiceCourseDetails, setVoiceCourseDetails] = useState({})

// fetch the course data
  useEffect(() => {
    axios.get('https://private-e05942-courses22.apiary-mock.com/courses')
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.log(err))
  },[])

// get the course 'slug' 
    const getCourses = (slug) => {
      setSlug(slug) 
    }

// get each course
useEffect(() => {
  axios.get(`https://private-e05942-courses22.apiary-mock.com/courses/${slug}` )
    .then(res => {
      slug === 'full-stack-immersion' ? setFullStackCourseCourseDetails(res.data) : setVoiceCourseDetails(res.data)
    })
    .catch(err => console.log(err))
},[slug])



// console.log(`State of slug ${slug}`)

// console.log(fullStackCourseDetails)  
// console.log(voiceCourseDetails)  

let sharedData = {data, getCourses, slug, fullStackCourseDetails, voiceCourseDetails}


  return (
    <DataContext.Provider value={sharedData}>
      {props.children}
    </DataContext.Provider>
  ) 
}



export  {DataContextProvider, DataContext}