
### Boilerplate
* `npx` create-react-app

### NPM packages 

* react-router-dom
* axios
* node-sass
* react-bootstrap


### 

I've decided to use React combined with hooks and the context API for this coding challenge. For fetching the data, I've used Axios. For styling, I've used react-bootstrap and node-sass. In the first 3 hours, I've managed to fetch the API's data and make a basic styling. I've made a commit to indicate the end of the 3 hours, but I've decided to work more on the task because I wanted to make it work. I have difficulties accessing the data with the course details. I've spent around 4 hours trying to get the correct course details to realize that the course description is the same for both courses :D. If I had more time, I would have loved to add more style to the pages to make them look closer to the careerfoundry ones. I was going to separate the context into multiple small ones and fetch the data for every component that needs it.